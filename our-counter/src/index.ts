import { ITracker } from './interfaces/tracker';
import { Tracker } from './Tracker';

declare global {
    interface Window {
        tracker: ITracker;
    }
}

(function () {
    const URL = 'http://localhost:8001/track';
    const SENDING_INTERVAL = 1000;

    console.log('start tracking');
    window.tracker = new Tracker(URL, SENDING_INTERVAL);
})();
