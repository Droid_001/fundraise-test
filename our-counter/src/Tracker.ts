import { ITrackedInformation, ITracker, tag, timestamp } from './interfaces/tracker';

export class Tracker implements ITracker {
    constructor(url: string, intervaInMs: number) {
        this.URL = url;
        this.SENDING_INTERVAL = intervaInMs;

        setInterval(this.sendEvents, this.SENDING_INTERVAL);
        window.addEventListener('unload', this.sendEventsSync);
    }

    private readonly URL: string;
    private readonly SENDING_INTERVAL: number;
    private eventsList: { [key: string]: ITrackedInformation } = {};
    private counterForId = 0;

    public track = async (event: string, ...tags: tag[]): Promise<void> => {
        if (!event) return;
        this.eventsList[this.uniqueIdGenerator()] = {
            event,
            tags,
            url: window.location.href,
            title: document.title,
            ts: this.getTimestamp(),
        };

        if (Object.keys(this.eventsList).length > 2) await this.sendEvents();
    };

    private sendEvents = async (): Promise<void> => {
        const keys = Object.keys(this.eventsList);
        if (!keys.length) return console.log('No tracked events: sending canceled');

        try {
            const response = await fetch(this.URL, {
                method: 'POST',
                mode: 'no-cors',
                headers: {
                    'content-type': 'text/plain',
                },
                body: JSON.stringify(Object.values(this.eventsList)),
            });
            if (response.status)
                return console.error('Events sending failed: ', response.statusText);

            console.log(`Evenets ${keys.join(', ')} sended succesfully: removing from buffer`);
            keys.forEach(key => {
                delete this.eventsList[key];
            });
        } catch (e) {
            console.error('catch', e);
        }
    };
    // Синхронная отправка перед хакрытием браузера
    private sendEventsSync = () => {
        if (Object.keys(this.eventsList).length)
            navigator.sendBeacon(this.URL, JSON.stringify(Object.values(this.eventsList)));
    };

    private getTimestamp = (): timestamp => {
        const date = new Date();
        return date.toISOString().replace(
            'Z',
            `+${Math.abs(date.getTimezoneOffset() / 60)
                .toString()
                .padStart(2, '0')}:00`,
        );
    };

    private uniqueIdGenerator = (): string => {
        ++this.counterForId;
        return `id_${this.counterForId}`;
    };
}
