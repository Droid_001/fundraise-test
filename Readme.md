# Структура каталогов

## our-counter/
Код счетчика с простеньким конфигом для вебпака. Есть свой MD файл

## app/
Express приложение, запускает инстансы для портов 8000 и 8001. Есть свой MD файл

# Сложности
По требованиям задачи сложностей не было. Разве что пришлось повозится с установкой Mongo на сервер - ip из России блокируют
Попробовал для себя впервые mongodb с typeorm.
По времени ~ 4 часов. Большая часть времени ушла на инфраструктурные работы.
