import { IsArray, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ITrackedInformation, tag, timestamp, event } from '~/types/tracked-information.interface';

export class TracketInformationCreateData implements ITrackedInformation {
    event: event;
    tags: tag[];
    url: string;
    title: string;
    ts: timestamp;
}

export class TrackedInformationCreateList {
    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => TracketInformationCreateData)
    body: ITrackedInformation[];
}
