export type event = string;
export type tag = string;
export type timestamp = string;

export interface ITracker {
    track(event: event, ...tags: tag[]): void;
}

export interface ITrackedInformation {
    event: event;
    tags: tag[];
    url: string;
    title: string;
    ts: timestamp;
}
