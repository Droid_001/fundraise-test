import { DataSource } from 'typeorm';
import { ormConfig } from '@/ormconfig';
import { logger } from '~/logger/logger';

export class DBConnection {
    static async connectDB(): Promise<any> {
        try {
            const dataSource = new DataSource(ormConfig);
            global.dataSource = dataSource;
            
            return dataSource.initialize();
        } catch (e) {
            logger.error(`Database connection error: ${e}`);
        }
    }
}
