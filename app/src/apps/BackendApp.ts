import 'reflect-metadata';
import bodyParser from 'body-parser';
import { useExpressServer } from 'routing-controllers';

import { AppCreator } from '@/apps/AppCreator';

import { stream } from '@/logger/middlewares/stream.middleware';
import { TrackedInformtionController } from '~/controllers/tracked-information.controller';

export class BackendApp extends AppCreator {
    async configureApp(): Promise<void> {
        this.app.use(stream, bodyParser.json());

        useExpressServer(this.app, {
            controllers: [TrackedInformtionController],
        });
    }
}
