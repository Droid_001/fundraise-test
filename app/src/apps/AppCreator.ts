import express, { Express } from 'express';
import { logger } from '@/logger/logger';
import { Server } from 'http';

export abstract class AppCreator {
    constructor(host: string, port: number) {
        [this.host, this.port] = [host, port];

        this.app = express();

        this.configureApp();

        this.server = this.app.listen(port, host, () =>
            logger.info(`app started on http://${this.host}:${this.port}`),
        );
    }

    readonly app: Express;
    readonly server: Server;

    private readonly host: string;
    private readonly port: number;

    abstract configureApp(): void;
}
