import { AppCreator } from '@/apps/AppCreator';
import { resolve } from 'path';
import { Request, Response } from 'express';

const OUR_PAGE_PATH = resolve('./static/page.html');

const sendOurPage = (req: Request, res: Response) => {
    res.sendFile(OUR_PAGE_PATH);
};

export class FrontendApp extends AppCreator {
    configureApp(): void {
        this.app.get('/', sendOurPage);
        this.app.get('/1.html', sendOurPage);
        this.app.get('/2.html', sendOurPage);

        this.app.use('*', (req, res) => {
            res.status(404).json({ message: 'Not found' });
        });
    }
}
