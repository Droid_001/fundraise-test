import { Controller, Res, Get, Post, Body, UseBefore } from 'routing-controllers';
import 'reflect-metadata';
import { response, Response } from 'express';
import { resolve } from 'path';
import { promisify } from 'util';

import { TrackedInformationCreateList } from '@/dto/tracker-information-list.dto';
import { TrackedInformationModel } from '~/models/tracked-information.model';
import bodyParser from 'body-parser';
import { arrayBodyToObjectMiddleware } from '~/middlewares/array-body-to-object.middleware';

@Controller()
export class TrackedInformtionController {
    private readonly TRACKER_SCRIPT = resolve(__dirname, '../../static/our-counter-build.js');

    private readonly trackedInformationModel = new TrackedInformationModel();

    @Get('/')
    async sendCounterScript(@Res() response: Response) {
        await promisify<string, void>(response.sendFile.bind(response))(this.TRACKER_SCRIPT);
        return response;
    }

    @UseBefore(
        bodyParser.json({
            type: ['text/plain'],
        }),
        arrayBodyToObjectMiddleware,
    )
    @Post('/track')
    async rent(
        @Res() response: Response,
        @Body({ validate: false }) body: TrackedInformationCreateList,
    ) {
        response.sendStatus(200);
        await this.trackedInformationModel.addMany(body.body);
        return response;
    }
}
