import morgan from 'morgan';
import { logger } from '@/logger/logger';
import { MORGAN_LOG_FORMAT } from '@/logger/constants';
import config from 'config';
import { MorganModes } from '../types';

const MORGAN_MODE = config.get<MorganModes>('MORGAN_MODE');

export const stream = morgan(MORGAN_LOG_FORMAT, {
    stream: {
        write: message => {
            logger[MORGAN_MODE](message.substring(0, message.lastIndexOf('\n')));
        },
    },
});
