export const MORGAN_LOG_FORMAT =
    ':remote-addr - :method :url HTTP/:http-version :status :res[content-length] :referrer :user-agent - :response-time ms';
