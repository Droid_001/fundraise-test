import { format, createLogger, transports } from 'winston';
import moment from 'moment';
import config from 'config';

export const logger = createLogger({
    level: config.get('WINSTON_DEBUG_LEVEL'),
    format: format.combine(
        format.timestamp(),
        format.errors({ stack: true }),
        format.splat(),
        format.colorize(),
        format.printf(info => {
            const { timestamp, level, message, ...args } = info;
            return `${moment(timestamp).format(
                'DD.MM.YYYY hh:mm:ss',
            )} ${level}: ${message}${
                Object.keys(args).length ? JSON.stringify(args, null, 2) : ''
            }`;
        }),
    ),
    transports: [new transports.Console()],
    exitOnError: false,
});