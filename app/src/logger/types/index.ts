export enum MorganModes {
    debug = 'debug',
    combined = 'combined',
    common = 'common',
    dev = 'dev',
    short = 'short',
}
