import { logger } from '@/logger/logger';
import config from 'config';

import { BackendApp } from '@/apps/BackendApp';
import { FrontendApp } from '@/apps/FrontendApp';
import { DBConnection } from './apps/DBConnection';

const BACKEND_PORT = config.get<number>('BACKEND_PORT');
const BACKEND_HOST = config.get<string>('BACKEND_HOST');

const FRONTEND_PORT = config.get<number>('FRONTEND_PORT');
const FRONTEND_HOST = config.get<string>('FRONTEND_HOST');

DBConnection.connectDB().then(() => {

    new FrontendApp(FRONTEND_HOST, FRONTEND_PORT);
    new BackendApp(BACKEND_HOST, BACKEND_PORT);

    process.on('unhandledRejection', err => {
        if (err) logger.error(err);
    });

    process.on('rejectionHandled', err => {
        if (err) logger.error(err);
    });

    process.on('uncaughtException', err => {
        if (err) logger.error(err);
    });
});
