import { TrackedInformationEntity } from '~/db/entities/tracked-information.entity';
import { Singleton } from '~/decorators/singleton.decorator';
import { logger } from '~/logger/logger';
import { ITrackedInformation } from '~/types/tracked-information.interface';
import { Model } from './Model';

@Singleton
export class TrackedInformationModel extends Model<TrackedInformationEntity> {
    constructor() {
        super(TrackedInformationEntity);
    }

    public async addMany(tracks: ITrackedInformation[]) {
        try {
            return await this.entity.insert(tracks);
        } catch (e) {
            logger.error('Cant save tracked information', e);
        }
    }
}
