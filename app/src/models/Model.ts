import { EntityTarget, Repository } from 'typeorm';

export class Model<T> {
    constructor(model: EntityTarget<T>) {
        this.entity = global.dataSource.getRepository(model);
    }

    readonly entity: Repository<T>;
}
