import { DataSourceOptions } from 'typeorm';
import config from 'config';

export const ormConfig: DataSourceOptions = {
    type: config.get<any>('DATABASE_TYPE'),
    host: config.get<any>('DATABASE_DOMAIN'),
    port: config.get<any>('DATABASE_PORT'),
    username: config.get<string>('DATABASE_USER'),
    password: config.get<string>('DATABASE_PASSWORD'),
    database: config.get<string>('DATABASE_NAME'),
    synchronize: true,
    logging: false,
    entities: ['src/db/entities/**/*.entity.ts'],
    migrations: ['src/db/migrations/**/*.ts'],
    subscribers: ['src/db/subscribers/**/*.ts'],
    useUnifiedTopology: true,
};
