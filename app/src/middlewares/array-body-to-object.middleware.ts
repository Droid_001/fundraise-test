import { Request, Response, NextFunction } from 'express';

export const arrayBodyToObjectMiddleware = (
    request: Request,
    response: Response,
    next: NextFunction,
): void => {
    if (Array.isArray(request.body))
        request.body = {
            body: request.body,
        };

    next();
};
