import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm';
import { ITrackedInformation } from '~/types/tracked-information.interface';

@Entity('trackedInformation')
export class TrackedInformationEntity implements ITrackedInformation {
    @ObjectIdColumn()
    uuid: ObjectID;
    @Column()
    event: string;
    @Column()
    tags: string[];
    @Column()
    url: string;
    @Column()
    title: string;
    @Column()
    ts: string;
}
